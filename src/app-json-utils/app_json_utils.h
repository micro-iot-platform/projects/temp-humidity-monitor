/*
 * File Name: app_json_utils.h
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#ifndef APP_JSON_UTILS_H_
#define APP_JSON_UTILS_H_

#include "../temp-humidity-app/temp_humidity_app.h"

/*
 * Creating Application POST Json String:
 *
 {
 	 "temperature_data" :
 	 	 {
 	 	 	 "pub_temp_in_celsius": 50,
 	 	 	 "pub_temp_in_fahrenheit": 122
 	 	 }
 }
 *
 *
 */
em_err make_temp_publish_json(TEMP_HUMIDITY_PUBLISH_DATA *publish_data, bool temp_publish_in_celsius,
		bool temp_publish_in_fahrenheit, char **ppbuf, size_t *plen);


/*
 * Creating Application POST Json String:
 *
 {
 	 "humidity_data":
 	 	 {
 	 	 	 "pub_humidity": 50
 	 	 }
 }
 *
 *
 */
em_err make_humidity_publish_json(TEMP_HUMIDITY_PUBLISH_DATA *publish_data, char **ppbuf, size_t *plen);


/*
 * Parse Application Config's response Json String:
 * {
 * 	"publish_temp" : true,
 * 	"publish_humidity" : true,
 * 	"read_dht22_inteval" : 500,
 * 	"temp_publish_interval" : 1000,
 * 	"humidity_publish_interval" : 2000,
 * 	"temp_publish_in_celsius" : true,
 * 	"temp_publish_in_fahrenheit" : true
 * }
 *
 */

em_err parse_config_json(char *string, TEMP_HUMIDITY_APP_CONFIGURATION_DATA *resp) ;

#endif /* APP_JSON_UTILS_H_ */
