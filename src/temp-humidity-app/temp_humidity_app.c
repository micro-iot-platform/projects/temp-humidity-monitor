/*
 * File Name: temp_humidity_app.c
 * File Path: /temp-humidity-monitor/src/temp-humidity-app/temp_humidity_app.c
 * Description:
 *
 *  Created on: 07-Aug-2019
 *      Author: Noyel Seth
 */

#include <math.h>
#include "temp_humidity_app.h"
#include "../app-json-utils/app_json_utils.h"
//#include "../dht22-lib/dht22.h"
#include "conn.h"
#include "appconfig_helper.h"
#include "apppostdata_helper.h"
#include "dht22.h"

#define TAG "temp_humidity_app"

TEMP_HUMIDITY_APP_CONFIGURATION_DATA app_config_data;

TEMP_HUMIDITY_PUBLISH_DATA publish_data;

char *temp_pub_json_buff = NULL;
size_t temp_pub_json_buff_len = 0;

char *humidity_pub_json_buff = NULL;
size_t humidity_pub_json_buff_len = 0;

TaskHandle temperature_publish_task_handle = NULL;
TaskHandle humidity_publish_task_handle = NULL;

#define DEFAULT_CONFIG_JSON	"{\"publish_temp\":1,\"publish_humidity\":1,\"read_dht22_interval\":2500,\"temp_publish_interval\":10000,\"humidity_publish_interval\":10000,\"temp_publish_unit\":2}"

static em_err app_configuration_process(char *app_config, size_t app_config_len) {
	em_err res = EM_FAIL;
	if (app_config != NULL) {
		EM_LOGI(TAG, "Temperature Humidity Monitor Application Configuration %s", app_config);
		res = parse_config_json(app_config, &app_config_data);
		if (res != EM_OK) {
			EM_LOGE(TAG, "Failed to parse App Configuration");
		}
	}
	return res;
}

static void temperature_cal_publish_task(void *param) {
	while (1) {
		if (app_config_data.publish_temp == true) {
			publish_data.temperature_data.temperature_in_celsius = dht22_get_temperature();//getTemperature();
			publish_data.temperature_data.temperature_in_fahrenheit =
					(publish_data.temperature_data.temperature_in_celsius * 9 / 5) + 32;

			em_err err = make_temp_publish_json(&publish_data, app_config_data.temp_publish_in_celsius,
					app_config_data.temp_publish_in_fahrenheit, &temp_pub_json_buff, &temp_pub_json_buff_len);
			if (err == EM_OK) {
				EM_LOGD(TAG, "*** Temperature Publish Start ***");
				EM_LOGD(TAG, "\r\n-: Temperature Publish JSON :-\r\n %s\r\n", temp_pub_json_buff);

				em_err res = migcloud_post_app_json_http(temp_pub_json_buff);
				if (res == EM_OK) {
//						EM_LOGI(TAG, "Temperature Data Publish Success");
				} else if (res == EM_ERR_NO_MEM) {
					EM_LOGE(TAG, "Temperature Data Publish Failed because of low memory");
				} else {
					EM_LOGE(TAG, "Temperature Data Publish Failed");
				}
				EM_LOGD(TAG, "*** Temperature Publish Finish ***");
			}
			free(temp_pub_json_buff);
			temp_pub_json_buff = NULL;
			TaskDelay(app_config_data.temp_publish_interval / TICK_RATE_TO_MS);
		} else {
			break;
		}
	}
	EM_LOGI(TAG, "Stop Temperature Publish Task '%s'", __func__);
	temperature_publish_task_handle = NULL;
	TaskDelete(NULL);
}

static void humidity_cal_publish_task(void *param) {
	while (1) {
		if (app_config_data.publish_humidity == true) {

			publish_data.humidity_data.humidity = dht22_get_humidity();//getHumidity();

			em_err err = make_humidity_publish_json(&publish_data, &humidity_pub_json_buff,
					&humidity_pub_json_buff_len);
			if (err == EM_OK) {
				EM_LOGD(TAG, "### Humidity Publish Start ###");
				EM_LOGD(TAG, "\r\n-: Humidity Publish JSON :-\r\n %s\r\n", humidity_pub_json_buff);

				em_err res = migcloud_post_app_json_http(humidity_pub_json_buff);
				if (res == EM_OK) {
//					EM_LOGI(TAG, "Humidity Data Publish Success");
				} else if (res == EM_ERR_NO_MEM) {
					EM_LOGE(TAG, "Humidity Data Publish Failed because of low memory");
				} else {
					EM_LOGE(TAG, "Humidity Data Publish Failed");
				}
				EM_LOGD(TAG, "### Humidity Publish Finish ###");
			}
			free(humidity_pub_json_buff);
			humidity_pub_json_buff = NULL;
			TaskDelay(app_config_data.humidity_publish_interval / TICK_RATE_TO_MS);
		} else {
			break;
		}
	}

	EM_LOGI(TAG, "Deleting %s", __func__);
	humidity_publish_task_handle = NULL;
	TaskDelete(NULL);
}

static void init_default_temp_humidity_app_configuration() {
	// set default Project Configuration
	em_err res = parse_config_json(DEFAULT_CONFIG_JSON, &app_config_data);
	if (res != EM_OK) {
		EM_LOGE(TAG, "Failed to parse Default App Configuration");
	}

	EM_LOGD(TAG, "Default DHT22 Read Interval Time-Duration: %d\n", app_config_data.read_dht22_interval);

	EM_LOGD(TAG, "Default Temperature Publish Status %d\n", app_config_data.publish_temp);
	EM_LOGD(TAG, "Default Temperature Publish Interval Duration %d\n", app_config_data.temp_publish_interval);
	EM_LOGD(TAG, "Default Temperature Publish Unit %d\n", app_config_data.temp_publish_unit);

	EM_LOGD(TAG, "Default Humidity Publish Status %d\n", app_config_data.publish_humidity);
	EM_LOGD(TAG, "Default Humidity Publish Interval Duration %d\n", app_config_data.humidity_publish_interval);
}

void temp_humidity_init() {
	// set the DHT22 Data out GPIO to the DHT22-lib
//	setDHTgpio(DHT22_DOUT_GPIO);
	dht22_set_dout_gpio(DHT22_DOUT_GPIO);

	// register application configuration function
	migcloud_register_app_config_function(app_configuration_process);

	// Set Default App Configurations
	init_default_temp_humidity_app_configuration();
}

void temp_humidity_loop() {

	int ret = dht22_read();//readDHT();
//	errorHandler(ret);
	dht22_error_handler(ret);

	if (temperature_publish_task_handle == NULL) {
		if (app_config_data.publish_temp == true) {
			bool status = TaskCreate(temperature_cal_publish_task, "temp_publish_task", TASK_STACK_SIZE_4K, NULL,
					THREAD_PRIORITY_5, &temperature_publish_task_handle);
			if (status == true) {
				EM_LOGI(TAG, "Temperature Publish task Started");
			} else {
				EM_LOGE(TAG, "Failed to start Temperature Publish task");
			}
		}
	}

	if (humidity_publish_task_handle == NULL) {
		if (app_config_data.publish_humidity == true) {
			bool status = TaskCreate(humidity_cal_publish_task, "humidity_publish_task", TASK_STACK_SIZE_4K, NULL,
					THREAD_PRIORITY_5, &humidity_publish_task_handle);
			if (status == true) {
				EM_LOGI(TAG, "Humidity Publish task Started");
			} else {
				EM_LOGE(TAG, "Failed to start Humidity Publish task");
			}
		}
	}

	TaskDelay(app_config_data.read_dht22_interval / TICK_RATE_TO_MS);
}
