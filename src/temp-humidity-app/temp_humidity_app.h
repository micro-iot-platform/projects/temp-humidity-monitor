/*
 * File Name: temp_humidity_app.h
 * File Path: /temp-humidity-monitor/src/temp-humidity-app/temp_humidity_app.h
 * Description:
 *
 *  Created on: 07-Aug-2019
 *      Author: Noyel Seth
 */

#ifndef TEMP_HUMIDITY_APP_H_
#define TEMP_HUMIDITY_APP_H_

#include "thing.h"
#include "emmate.h"
#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"

typedef enum {
	TEMPERATURE_PUBLISH_IN_CELSIUS = 0,
	TEMPERATURE_PUBLISH_IN_FAHRENHEIT,
	TEMPERATURE_PUBLISH_IN_CELSIUS_AND_FAHRENHEIT,
} TEMPERATURE_PUBLISH_UNIT;

typedef struct {
	bool publish_temp;
	bool publish_humidity;

	uint32_t read_dht22_interval;

	uint32_t temp_publish_interval;
	uint32_t humidity_publish_interval;

	uint16_t temp_publish_unit;

	bool temp_publish_in_celsius;
	bool temp_publish_in_fahrenheit;

} TEMP_HUMIDITY_APP_CONFIGURATION_DATA;

typedef struct temeprature_data {
	float temperature_in_celsius;
	float temperature_in_fahrenheit;
} TEMPERATURE_DATA;

typedef struct humidity_data {
	float humidity;
} HUMIDITY_DATA;

typedef struct {
	TEMPERATURE_DATA temperature_data;
	HUMIDITY_DATA humidity_data;
} TEMP_HUMIDITY_PUBLISH_DATA;

void temp_humidity_init();

void temp_humidity_loop();

#endif /* TEMP_HUMIDITY_APP_H_ */
