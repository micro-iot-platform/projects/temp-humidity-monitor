#include "temp-humidity-app/temp_humidity_app.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void *param) {
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	EM_LOGI(TAG, "");
	EM_LOGI(TAG, "==================================================================");

	temp_humidity_init();

	while (1) {
		temp_humidity_loop();
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
	}
}
